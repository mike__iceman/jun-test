from test.api import app
from test.file_worker import csv_reader


CSV_PATH = '.\\static\\recommends.csv'

if __name__ == '__main__':
    csv_reader.read(CSV_PATH)
    app.run()