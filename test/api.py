from flask import Flask, Response
from flask import request

from test.file_worker import csv_reader


app = Flask(__name__)


@app.route('/')
def hello():
    sku = request.args.get('sku')
    if not sku:
        return Response('`sku` parameter should not be empty')

    rank = request.args.get('rank')
    if rank:
        try:
            rank = float(rank)
        except:
            return Response('`rank` parameter should be float')

    recommended = csv_reader.find(sku=sku, rank=rank)
    return Response(str(recommended))