import os
import sys
import itertools
from collections import defaultdict


class CSVReader:
    def __init__(self):
        self.filepath = None
        self.csv_map = defaultdict(list)
        self.csv_max_line_len = None

    def read(self, filepath):
        if not filepath or not os.path.exists(filepath):
            print(f'File `{os.path.basename(filepath)}` not exists in static folder. Please put the file in the correct folder.')
            sys.exit()

        self.filepath = filepath

        with open(self.filepath, 'r') as file:
            for idx, line in enumerate(file):
                if idx == 0:
                    self.csv_max_line_len = len(line)

                sku, _, _ = line.split(',')
                self.csv_map[sku].append(idx)

                if idx % 1_000_000 == 0:
                    print(f'Loading {idx} sku', end='\r')

        print(f'Sucessfully loaded {idx + 1} records with {len(self.csv_map.keys())} unique sku`s')


    def find(self, sku, rank=None):
        if not self.filepath:
            print('You should read file first')

        result = []

        idx_list = self.csv_map[sku]
        with open(self.filepath, 'r') as file:
            for idx in idx_list:
                line = self._read_line(file, index=idx)
                csv_sku, csv_rec_sku, csv_rank = line.split(',')
            
                if rank and rank > float(csv_rank):
                    continue

                result.append(csv_rec_sku)

        return result
    

    def _read_line(self, file, index):
        if self.csv_max_line_len is None:
            print('You should read file first')
            return

        file.seek(index*self.csv_max_line_len)
        return file.readline().strip()


csv_reader = CSVReader()